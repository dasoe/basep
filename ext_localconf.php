<?php

// Prevent Script from being called directly
defined('TYPO3') or die();

// encapsulate all locally defined variables
(function () {
    // RTE Preset
    $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['dasoeDefault'] = 'EXT:basep/Configuration/RTE/Default.yaml';
    $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['dasoeTitle'] = 'EXT:basep/Configuration/RTE/Title.yaml';

    
    // Default User TsConfig    
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
        "@import 'EXT:basep/Configuration/TsConfig/User/*.tsconfig'"
    );
    
    // BE stylesheets
    // $GLOBALS['TYPO3_CONF_VARS']['BE']['stylesheets']['ebersberg23'] = 'EXT:basep/Resources/Public/Css/Backend/';


})();

