<?php

/* PageContentViewHelper
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dasoe\Basep\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class InArrayViewHelper extends AbstractViewHelper
{
    
    public function initializeArguments()
{
    $this->registerArgument('singleValue', 'integer', 'singleValue', true);
    $this->registerArgument('manyValue', 'array', 'manyValue', true);
}

   public static function renderStatic(
       array $arguments,
       \Closure $renderChildrenClosure,
       RenderingContextInterface $renderingContext
   ) {
       
       return ( in_array( $arguments['singleValue'] , $arguments['manyValue'] )  );
                      
       
   }
}

