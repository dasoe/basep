<?php

namespace Dasoe\Basep\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;


final class CountStringViewHelper extends AbstractViewHelper {

    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    public function initializeArguments() {
        // registerArgument($name, $type, $description, $required, $defaultValue, $escape)
        $this->registerArgument('string', 'string', 'string to be counted', true);
    }

    public static function renderStatic(
            array $arguments,
            \Closure $renderChildrenClosure,
            RenderingContextInterface $renderingContext
    ) {
        // this is improved with the TagBasedViewHelper (see below)
        return strlen($arguments['string']);
    }

}


