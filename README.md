# Base package

## Installation einer neuen Instanz

#### 1. Zeug holen

1. Typo3-Installation per composer helper
2. composer require symfony/dotenv helhum/dotenv-connector *
3. composer config repositories.basep vcs https://gitlab.com/dasoe/basep.git
4. composer require dasoe/basep:@dev

#### 2. Templates einsetzen
1. .env aus basep/Resources/Private/Server/.env_example in root folder .env **

`Comments dazu:
remove comments and place .env file on server, same level as composer.json
do "composer install" to load variables
variables from .env file are loaded on server by composer packages
symfony/dotenv and  helhum/dotenv-connector`

2. Additional Configuration aus basep/Resources/Private/Configuration/AdditionalConfiguration.php *** in public/typo3conf/AdditionalConfiguration.php

3. Site Settings aus basep/Resources/Private/Configuration/Site/\_default/config.yaml *** in root folder config/sites/_siteIdentifier_/config.yaml 

#### 3. Einstellungen machen


\* für .env  |  ** benötigt Extension, s. oben: 1.2.  |  *** liest Daten aus .env


## Einbinden von ke_search
Constants: 

`
plugin.tx_basep.searchBoxCEId = XXX # content element(not page/folder!) "Searchbox and Filters"
plugin.tx_basep.searchPageId = YYY # search page
`

Die Reihenfolge ist entscheidend. ke_search muss vor der basep eingebunden werden, da wir sie ja benutzen!


### Container:

- Collapse
- Simple
- 2Cols, 3 Versions
- 3Cols
- 4Cols

Alle mit angepasstem Page-Modul-BE-Partials für mögliches Styling: 
- TemplateOverrides/Partials/PageLayout/Record.html für den Content
- TemplateOverridesContainer/Partials/PageLayout/Record.html für den Content innerhaöb eines der Container
macht:
- Zusätzliche Klasse "type-{item.record.CType}" zu "t3-page-ce"
- Zusätzliche Ausgabe header-layout-X für header CType


---- old-----

## Menu

### CSS Media Queries

Are included. In case you want to use other breakpoints e.g., please change
Setup:

`
page.includeCSS.basequeries > 
\# original value: EXT:basep/Resources/Public/Css/BaseMediaQueries.css
...
`

### ke_search
(Note: search settings are done in Backend for each installation seperately and are not part of this extension: search page, search configuration folder and 'Indexers' as well as 'Filters' records. Also of course ke_search template has to be included seperately)

Searchbox in menu: not autmatically included by this extension, but prepared.

Constants: 

`
searchBoxCEId = XXX # content element "Searchbox and Filters"
searchPageId = YYY # page
`

Setup:

`
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:basep/Configuration/TypoScript/kesearch.typoscript">
`

This will include CSS, JS, adjusted Searchbox Template and variable for page.10 (FLUIDTEMPLATE) containing the searchbox plugin.



