<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Base Package',
    'description' => '',
    'category' => 'Base Package',
    'author' => 'das oe',
    'author_email' => 'christian.oettinger@gmx.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'container' => '2.1.0-2.1.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
