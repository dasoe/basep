<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;


defined('TYPO3') || die();


ExtensionManagementUtility::registerPageTSConfigFile(
   'basep',
   'Configuration/TsConfig/Page/default.tsconfig',
   'basep default settings'
);
ExtensionManagementUtility::registerPageTSConfigFile(
   'basep',
   'Configuration/TsConfig/Page/BackendLayouts.tsconfig',
   'Backend Layouts'
);

ExtensionManagementUtility::registerPageTSConfigFile(
   'basep',
   'Configuration/TsConfig/Page/BackendLayouts2Cols.tsconfig',
   'Backend Layouts: 2 Columns'
);

ExtensionManagementUtility::registerPageTSConfigFile(
   'basep',
   'Configuration/TsConfig/Page/BackendLayoutsFooter.tsconfig',
   'Backend Layouts with Footer'
);


ExtensionManagementUtility::registerPageTSConfigFile(
   'basep',
   'Configuration/TsConfig/Page/BackendLayoutsHomePage.tsconfig',
   'Backend Layouts: Home Page'
);