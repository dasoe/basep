<?php
defined('TYPO3') || die();

// TYPO3 12, no more TECFORM override for default value (empty) actually a bug https://forge.typo3.org/issues/100775
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][0]['label'] = 'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:space_classes.default';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][0]['label'] = 'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:space_classes.default';

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'basep-fold-container', // CType
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:foldable_container.label',              
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:foldable_container.decription',              
            [
                [
                    ['name' => 'content', 'colPos' => 201],
                ]
            ] // grid configuration
        )
    )
    // set an optional icon configuration
    ->setIcon('EXT:basep/Resources/Public/Icons/fold_bl.svg')
    ->addGridPartialPath('EXT:basep/Resources/Private/TemplateOverridesContainer/Partials/')
        
);

$GLOBALS['TCA']['tt_content']['types']['basep-fold-container']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general, --palette--;;general, header;LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:foldable_container.header, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance, --palette--;;frames, --palette--;;appearanceLinks, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language, --palette--;;language, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, --palette--;;hidden, --palette--;;access, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories, categories, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes, rowDescription, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,';


\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'basep-simple-container', // CType
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:simple_container.label',              
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:simple_container.decription',              
                
            [
                [
                    ['name' => 'content', 'colPos' => 201],
                ]
            ] // grid configuration
        )
    )
    // set an optional icon configuration
    ->setIcon('EXT:basep/Resources/Public/Icons/1column_bl.svg')
    ->addGridPartialPath('EXT:basep/Resources/Private/TemplateOverridesContainer/Partials/')
        
);

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'basep-2cols-container', // CType
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:2cols_container.label',              
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:2cols_container.decription',              
                
            [
                [
                    ['name' => 'left side', 'colPos' => 201],
                    ['name' => 'right side', 'colPos' => 202]
                ]
            ] // grid configuration
        )
    )
    // set an optional icon configuration
    ->setIcon('EXT:basep/Resources/Public/Icons/2columns_bl.svg')
    ->addGridPartialPath('EXT:basep/Resources/Private/TemplateOverridesContainer/Partials/')
);


\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'basep-2cols-container_33-66', // CType
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:2cols_1-2_container.label',              
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:2cols_1-2_container.decription',              
            [
                [
                    ['name' => 'left side', 'colPos' => 201],
                    ['name' => 'right side', 'colPos' => 202]
                ]
            ] // grid configuration
        )
    )
    // set an optional icon configuration
    ->setIcon('EXT:basep/Resources/Public/Icons/2columnsRightBig_bl.svg')
    ->addGridPartialPath('EXT:basep/Resources/Private/TemplateOverridesContainer/Partials/')
);


\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'basep-2cols-container_66-33', // CType
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:2cols_2-1_container.label',              
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:2cols_2-1_container.decription',              
            [
                [
                    ['name' => 'left side', 'colPos' => 201],
                    ['name' => 'right side', 'colPos' => 202]
                ]
            ] // grid configuration
        )
    )
    // set an optional icon configuration
    ->setIcon('EXT:basep/Resources/Public/Icons/2columnsLeftBig_bl.svg')
    ->addGridPartialPath('EXT:basep/Resources/Private/TemplateOverridesContainer/Partials/')
);


\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'basep-3cols-container', // CType
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:3cols_container.label',              
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:3cols_container.decription',              
            [
                [
                    ['name' => 'left', 'colPos' => 201],
                    ['name' => 'middle', 'colPos' => 202],
                    ['name' => 'right', 'colPos' => 203],
                ]
            ] // grid configuration
        )
    )
    // set an optional icon configuration
    ->setIcon('EXT:basep/Resources/Public/Icons/3columns_bl.svg')
    ->addGridPartialPath('EXT:basep/Resources/Private/TemplateOverridesContainer/Partials/')
);

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
        new \B13\Container\Tca\ContainerConfiguration(
            'basep-4cols-container', // CType
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:4cols_container.label',              
            'LLL:EXT:basep/Resources/Private/Language/locallang_be.xlf:4cols_container.decription',              
            [
                [
                    ['name' => 'comlumn 1', 'colPos' => 201],
                    ['name' => 'comlumn 2', 'colPos' => 202],
                    ['name' => 'comlumn 3', 'colPos' => 203],
                    ['name' => 'comlumn 4', 'colPos' => 204]
                ]
            ] // grid configuration
        )
    )
    // set an optional icon configuration
    ->setIcon('EXT:basep/Resources/Public/Icons/4columns_bl.svg')
    ->addGridPartialPath('EXT:basep/Resources/Private/TemplateOverridesContainer/Partials/')
);