<?php

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

return [
    'content-text-columns' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:basep/Resources/Public/Icons/content-text-columns2.svg'
    ],
    'content-homepage' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:basep/Resources/Public/Icons/content-homepage.svg'
    ],    
    'content-withfooter' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:basep/Resources/Public/Icons/content-withfooter.svg'
    ],    
    'content-withheader' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:basep/Resources/Public/Icons/content-withheader.svg'
    ],    
    
];
