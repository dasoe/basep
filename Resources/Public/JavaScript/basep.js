let inited = false;
let lastIsMobile;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$( document ).ready( function () {

    console.log( "basep ready" );



    // respectice stuff for margin of header
    $( '.collapsableHeader' ).each( function ( event, data ) {
        $( this ).data( 'orMarBot', parseInt( $( this ).css( 'margin-bottom' ) ) );
        // then set height to 0
        $( this ).css( {
            'margin-bottom': 0
        } );
    } );


    // ####### add this line to your js if you are ready with preparing/altering menus.
    // initMobileMenu();

    //initMenus();

//    $.tooltipster.setDefaults({
//        side: 'bottom'       
//    });
//    $('.tooltip').tooltipster({
//            theme: ['tooltipster-light','tooltipster-light-customized'],
//            functionPosition: function(instance, helper, position){
//                position.coord.top -= 45;
//                position.coord.left -= 15;
//                return position;
//            }
//    });

} );


// do the same in case window is resized
$( window ).resize( function () {
    initMenus();

} );

$( window ).on( "load", function () {
    $( ".accordion" ).not( ".accordion.opened" ).accordion( {
        header: "header",
        active: false, // false means closed
        collapsible: true,
        heightStyle: "panel"
    } );
    $( ".accordion.opened" ).accordion( {
        header: "header",
        active: 0, // 0 means first panel!!
        collapsible: true,
        heightStyle: "panel"
    });
    
    
    $('.maccordion .maccordion-header').click(function(e) {
            e.preventDefault();

        let element = $(this);

        if (element.next().hasClass('show')) {
            element.next().removeClass('show');
            element.next().slideUp(350);
        } else {
            element.parent().parent().find('li .inner').removeClass('show');
            element.parent().parent().find('li .inner').slideUp(350);
            element.next().toggleClass('show');
            element.next().slideToggle(350);
        }
    });    
    
    
    $('.maccordionOnlyOne .maccordion-header').click(function(e) {
            e.preventDefault();

        let element = $(this);
        
        element.parent().find(".maccordion-header").each(function (event, data) {
            
            $(this).next().slideUp(350);
        });

        if (element.next().hasClass('show')) {
            element.next().removeClass('show');
            element.next().slideUp(350);
        } else {
            element.parent().parent().find('li .inner').removeClass('show');
            element.parent().parent().find('li .inner').slideUp(350);
            element.next().toggleClass('show');
            element.next().slideToggle(350);
        }
    });    
    
    
} );



function initMobileMenu() {

    console.log( "initMobileMenu" );
    /* 
     ****************** MOBILE MENU *************
     */

    //calculateMobileMenuHeights();

    // prepare sub information
    $( '#mobilemenuContent ul li' ).has( "ul.menuLevel2" ).each( function ( event, data ) {
        console.log( "add hasSub" );
        $( this ).addClass( "hasSub" );
        $( this ).children( 'a' ).addClass( "hasSub" );
    } );


    // menu opening/closing
    $( '.mobileMenuIcon' ).on( "click", function ( event, data ) {
        if ( $( this ).hasClass( 'open' ) ) {
            $( this ).removeClass( 'open' );
            closeMobileMenu();
        } else {
            $( this ).addClass( 'open' );
            openMobileMenu();
        }
    } );

    // submenu opening/closing
    $( 'li.hasSub .nav-toggle' ).on( "click", function ( event, data ) {
        //event.preventDefault();
        let menuItem = $( this ).parent();
        if ( menuItem.hasClass( 'open' ) ) {
            menuItem.removeClass( "open" );
            closeMobileSubMenu( menuItem.find( 'ul.submenu:first' ) );
        } else {
            menuItem.addClass( "open" );
            openMobileSubMenu( menuItem.find( 'ul.submenu:first' ) );
        }
    } );
}


function initMenus() {

    if ( !inited ||
            lastIsMobile != isMobile()
            ) {
        console.log( "######## NEW SETTING ########" );
        inited = true;
        lastIsMobile = isMobile();


        if ( isMobile() ) {
            // remove event handler of menus
            $( '#mainmenu li.level-0' ).off( "mouseover" );
            $( '#mainmenu li.level-1' ).off( "mouseover" );
            $( '#menuCloseOverlay' ).off( "mouseover" );

            $( '#mainmenu li.level-0' ).on( "click", function ( event, data ) {
                // event.preventDefault();



            } );

            $( '#mainmenu li.level-1' ).on( "click", function ( event, data ) {
                // event.preventDefault();


            } );

        } else {

            // remove event handler of menus
            $( '#mainmenu li.level-0' ).off( "click" );
            $( '#mainmenu li.level-1' ).off( "click" );
            $( '#menuCloseOverlay' ).off( "click" );


            $( '#mainmenu li.level-0' ).on( "mouseover", function ( event, data ) {
                if ( !$( this ).hasClass( "open" ) && $( this ).hasClass( "has-children" ) ) {
                    openSubMenu( $( this ), 2 );
                }
            } );
            $( '#mainmenu li.level-1' ).on( "mouseover", function ( event, data ) {
                if ( !$( this ).hasClass( "open" ) && $(this).hasClass("has-children") ) {
                    openSubMenu( $( this ), 3 );
                }
            } );
            $( '#menuCloseOverlay' ).on( "mouseover", function ( event, data ) {
                if ( !$( this ).hasClass( "open" ) ) {
                    $( ".menuLevel1" ).removeClass( "open" );
                    $( ".menuLevel2" ).removeClass( "open" );
                    $( ".menuLevel3" ).removeClass( "open" );
                    $( ".menuLevel1 li" ).removeClass( "open" );
                    $( ".menuLevel1" ).attr( "aria-expanded", "false" );
                    $( ".menuLevel2" ).attr( "aria-expanded", "false" );
                    $( ".menuLevel3" ).attr( "aria-expanded", "false" );
                    $( ".menuLevel1 li" ).attr( "aria-expanded", "false" );
                    $( this ).css( 'display', 'none' );
                }
            } );
            // close open Menu
            if ( $( '.mobileMenuIcon' ).hasClass( 'open' ) ) {
                $( '.mobileMenuIcon' ).removeClass( 'open' );
                closeMobileMenu();
            }
        }

        if ( typeof newSettingTriggered === "function" ) {
            newSettingTriggered();
        }

    }



}


function isMobile() {
    return $( "#isMobile" ).is( ':visible' );
}

function isTablet() {
    return $( '#isTablet' ).is( ':visible' );
}


function openSubMenu( element, level ) {
    console.log( "close stuff and open sub menu if available - level " + level );
    $( ".menuLevel" + level ).removeClass( "open" );
    $( ".menuLevel" + level ).attr( "aria-expanded", "false" );

    $( ".menuLevel" + ( level - 1 ) + " li" ).removeClass( "open" );
    $( ".menuLevel" + ( level - 1 ) + " li" ).attr( "aria-expanded", "false" );

    element.find( ".menuLevel" + level ).addClass( "open" );
    element.find( ".menuLevel" + level ).attr( "aria-expanded", "true" );

    $( '#menuCloseOverlay' ).css( "display", 'block' );

    element.addClass( "open" );
    element.attr( "aria-expanded", "true" );
}


function openMobileMenu() {
    console.log( "open mobile Menu" );
//    $( '#mobilemenuContent' ).velocity( {
//        height: $( '#mobilemenuContent' ).data( 'originalHeight' ),
//    }, 650, function () {
//        $( '#mobilemenuContent' ).css( 'height', 'auto' );
//    } );

    $( '#mobilemenuContent' ).slideDown( "slow", function() {
    // Animation complete.
    });

}

function closeMobileMenu() {
    console.log( "close mobile Menu" );
//    $( '#mobilemenuContent' ).velocity( {
//        height: 0,
//    }, 650, function () {
//    } );
    $( '#mobilemenuContent' ).slideUp( "slow", function() {
    // Animation complete.
    });


}

function openMobileSubMenu( element ) {
    console.log( "open mobile SUBMenu" );
//    console.log( element );
//    element.velocity( {
//        height: element.data( 'originalHeight' ),
//    }, 650, function () {
//        element.css( 'height', 'auto' );
//    } );
    element.slideDown( "slow", function() {
    // Animation complete.
    });

}


function closeMobileSubMenu( element ) {
    console.log( "close mobile SUBMenu" );
//    element.velocity( {
//        height: 0,
//    }, 650, function () {
//    } );
    element.slideUp( "slow", function() {
    // Animation complete.
    });
}


