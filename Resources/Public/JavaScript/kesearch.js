

$(document).ready(function () {

    $('#searchIcon').on("click", function (event, data) {
        if ($(this).hasClass('open')) {
            if ($('#searchboxInner #form_kesearch_pi1 #ke_search_sword').val()) {
                $('#searchboxInner #form_kesearch_pi1').submit();
            } else {
                closeSearchbox();
                console.log('clicked to close/send');
            }
        } else {
            openSearchbox();
        }
    });

});


function openSearchbox() {
    console.log('open searchbox');
    $('#searchboxItself').addClass('open');
    $('#searchIcon').addClass('open');
    $('#ke_search_sword').focus();
}

function closeSearchbox() {
    console.log('close searchbox');

    $('#searchboxItself').removeClass('open');
    $('#searchIcon').removeClass('open');

}

