<?php
if (\TYPO3\CMS\Core\Core\Environment::getContext()->isDevelopment() || getenv('IS_DDEV_PROJECT') == 'true') {
    $GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
        $GLOBALS['TYPO3_CONF_VARS'],
        [
            'BE' => [
                // show field names in backend
                'debug' => true,
            ],
            // This GFX configuration allows processing by installed ImageMagick 6
            'GFX' => [
                'processor' => 'ImageMagick',
                'processor_path' => '/usr/bin/',
                'processor_path_lzw' => '/usr/bin/',
            ],
            'SYS' => [
                // disable caching for development
                'caching' => [
                    'cacheConfigurations' => [
                        'core' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'hash' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'pages' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'pagesection' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'runtime' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'rootline' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'imagesizes' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'assets' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'l10n' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'fluid_template' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ],
                        'extbase' => [
                            'backend' => \TYPO3\CMS\Core\Cache\Backend\NullBackend::class
                        ]
                    ]
                ],
                'displayErrors' => 1,
                'sqlDebug' => 1,
            ]
        ]
    );
}

$GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
    $GLOBALS['TYPO3_CONF_VARS'],
    [
        // read DB data from environment variables
        'DB' => [
            'Connections' => [
                'Default' => [
                    'dbname' => \getenv('TYPO3_DB_NAME'),
                    'host' => \getenv('TYPO3_DB_HOST'),
                    'password' => \getenv('TYPO3_DB_PASSWORD'),
                    'port' => \getenv('TYPO3_DB_PORT'),
                    'user' => \getenv('TYPO3_DB_USER'),
                ],
            ],
        ],
        // read Mail settings from environment variables
        'MAIL' => [
            'transport' => \getenv('TYPO3_MAIL_TRANSPORT'),
            'transport_smtp_server' => \getenv('TYPO3_MAIL_TRANSPORT_SMTP_SERVER'),
            'transport_sendmail_command' => \getenv('TYPO3_MAIL_TRANSPORT_SENDMAIL_COMMAND'),
            'defaultMailFromAddress' => 'noreply@change.me',
            'defaultMailFromName' => 'Change defaultMailFromName',
            'defaultMailReplyToAddress' => 'info@change.me',
            'defaultMailReplyToName' => 'Change defaultMailReplyToName',
        ],
        // read site details (context specific) from environment variables
        'SYS' => [
            'ddmmyy' => 'd.m.Y',
            'devIPmask' => \getenv('TYPO3_DEV_IP_MASK'),
            'displayErrors' => 0,
            'sitename' => \getenv('TYPO3_SITENAME') . ' on ' . \getenv('TYPO3_CONTEXT'),
            'systemLocale' => 'de_DE.UTF-8',
            'trustedHostsPattern' => \getenv('TYPO3_TRUSTED_HOST_PATTERN'),
            'UTF8filesystem' => true,
        ]
    ]
);
